package ru.mineapi.common.nbt;

public interface INBTTagCompound {

	public boolean getBoolean(String key);
	public int getInt(String key);
	public float getFloat(String key);
	public double getDouble(String key);
	public String getString(String key);
	public INBTTagCompound getCompoundTag(String key);
	public INBTTagList getList(String key);
	
	public <T extends INBTTagCompound> T setBoolean(String key, boolean value);
	public <T extends INBTTagCompound> T setInteger(String key, int value);
	public <T extends INBTTagCompound> T setFloat(String key, float value);
	public <T extends INBTTagCompound> T setDouble(String key, double value);
	public <T extends INBTTagCompound> T setString(String key, String value);
	public <T extends INBTTagCompound> T setCompound(String key, INBTTagCompound value);
	public <T extends INBTTagCompound> T setList(String key, INBTTagList value);
}
