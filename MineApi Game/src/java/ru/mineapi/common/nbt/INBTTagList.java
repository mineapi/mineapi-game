package ru.mineapi.common.nbt;

public interface INBTTagList {

	public <T extends INBTTagList> T remove(int pos);
	public <T extends INBTTagList> T set(int pos, Object obj);
	public <T extends INBTTagList> T add(Object obj);
	
	public int size();
	public <T> T getElementAt(int pos);
	public INBTTagCompound getCompoundAt(int pos);
	
}
