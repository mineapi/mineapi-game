package ru.mineapi.common.block.material;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Материал блока <br>
 * Описывает некоторые общие для блоков свойства (горят ли и пр.) 
 * @author swayfarer
 *
 */
@Data @Accessors(fluent = true)
public class BlockMatetrial {

	/** Горящий ли материал? (как дерево, шерсть и пр.) */
	@InternalElement 
	public boolean isBurning;
	
	/** Заменяемый ли материал? */
	@InternalElement 
	public boolean isReplaceable;
	
	/** Сплошной ли материал? */
	@InternalElement 
	public boolean isSolid;
	
	/** Необходим ли инструмент для поломки блока? */
	@InternalElement 
	public boolean isToolRequired;
	
}
