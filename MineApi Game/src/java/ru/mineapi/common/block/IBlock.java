package ru.mineapi.common.block;

import java.util.List;
import java.util.Random;

import ru.mineapi.common.block.material.BlockMatetrial;
import ru.mineapi.common.entity.IEntity;
import ru.mineapi.common.entity.IEntityPlayer;
import ru.mineapi.common.explosion.IExplosion;
import ru.mineapi.common.world.IWorld;

/**
 * Интерфейс, описывающий блок <br>
 * @author swayfarer
 *
 */
public interface IBlock {

	/** Прозрачен ли блок? */
	public boolean isOpacueCube();
	
	/** Является ли блок полноценным блоком? Через них нельзя проходить */
	public boolean isNormalBlock();
	
	/** Получить силу освещения, которое излучает блок */
	public int getLightValue();
	
	/** Получить материал блока */
	public BlockMatetrial getMaterial();
	
	/**
	 * Насколько трудно сломать блок?
	 * @param IWorld Мир, в котором находится блок
	 * @param x X-координата блока
	 * @param y Y-координата блока
	 * @param z Z-координата блока
	 * @return Трудность поломки блока в
	 */
	public float getBlockHardness(IWorld IWorld, int x, int y, int z);
	
	/**
	 * Обработка обновления блока раз в тик
	 * @param world Мир, в котором находится блок
	 * @param x X-координата блока
	 * @param y Y-координата блока
	 * @param z Z-координата блока
	 * @param rand Генератор рандомных чисел
	 */
	public void updateTick(IWorld world, int x, int y, int z, Random rand);

   /**
	 * Вызываемое случайным образом отображение разной "косметики", вроде частиц и т.п.
	 * @param world
	 * @param world Мир, в котором находится блок
	 * @param x X-координата блока
	 * @param y Y-координата блока
	 * @param z Z-координата блока
	 * @param rand Генератор рандомных чисел
	 */
   public void randomDisplayTick(IWorld world, int x, int y, int z, Random rand);

   /**
	 * Игрок уничтожил блок
	 * @param world Мир, в котором находится блок
	 * @param x X-координата блока
	 * @param y Y-координата блока
	 * @param z Z-координата блока
	 * @param meta Метадата
	 */
   public void onBlockDestroyedByPlayer(IWorld world, int x, int y, int z, int meta);

   /**
  	 * Игрок уничтожил блок
  	 * @param world Мир, в котором находится блок
  	 * @param x X-координата измененного блока
  	 * @param y Y-координата измененного блока
  	 * @param z Z-координата измененного блока
  	 * @param changedBlock Блок, который был изменер
  	 */
   public void onNeighborBlockUpdated(IWorld world, int x, int y, int z, IBlock changedBlock);

   /**
  	 * Блок был добавлен в мир
  	 * @param world Мир, в котором находится блок
  	 * @param x X-координата блока
  	 * @param y Y-координата блока
  	 * @param z Z-координата блока
  	 */
   public void onBlockAdded(IWorld world, int x, int y, int z);

   /**
 	 * Блок был уничтожен чем-либо
 	 * @param world Мир, в котором находится блок
 	 * @param x X-координата блока
 	 * @param y Y-координата блока
 	 * @param z Z-координата блока
 	 * @param block Уничтоженный блок
 	 * @param meta Мета уничтоженного блока
 	 */
   public void onBlockBreaked(IWorld world, int x, int y, int z, IBlock block, int meta);

   /**
	 * Блок был уничтожен чем-либо
	 * @param world Мир, в котором находится блок
	 * @param x X-координата блока
	 * @param y Y-координата блока
	 * @param z Z-координата блока
	 * @param meta Мета уничтоженного блока
	 * @param entity Энити, которая пересеклась с блоком 
	 */
  public void onEntityCollidedWithBlock(IWorld world, int x, int y, int z, int meta, IEntity entity);
  
  /**
	* Блок был уничтожен чем-либо
	* @param world Мир, в котором находится блок
	* @param x X-координата блока
	* @param y Y-координата блока
	* @param z Z-координата блока
	* @param block Уничтоженный блок
	* @param meta Мета уничтоженного блока
	*/
  public void onBlockHarvested(IWorld world, IEntityPlayer player, int x, int y, int z, int fortuneLevel);
  
  /**
	* Блок был уничтожен чем-либо
	* @param world Мир, в котором находится блок
	* @param x X-координата блока
	* @param y Y-координата блока
	* @param z Z-координата блока
	* @param block Уничтоженный блок
	* @param meta Мета уничтоженного блока
	*/
  public boolean canBlockStay(IWorld world, int x, int y, int z);
  
  /**
	* Блок был уничтожен чем-либо
	* @param world Мир, в котором находится блок
	* @param x X-координата блока
	* @param y Y-координата блока
	* @param z Z-координата блока
	* @param side Сторона блока
	*/
  public int getRedstonePower(IWorld world, int x, int y, int z, int side);
  
  /** Может ли блок быть вскопан при помощи зачарования "Шелковое касание" */
  public boolean canSilkTouch();
  
  /** Получить отображаемое имя */
  public String getDisplayName();
  
  /** Получить имя, под которым зарегистрирован блок */
  public String getBlockName();
  
  /** Добавить отображаемые в креавтивном окне блоки */
  public void addSubBlocks(List<?> subBlocksList);
  
  /**
	* Блок был уничтожен взрывом
	* @param world Мир, в котором находится блок
	* @param x X-координата блока
	* @param y Y-координата блока
	* @param z Z-координата блока
	* @param explosion Взрыв, который уничтожил блок 
	*/
  public void onBlockExploded(IWorld world, int x, int y, int z, IExplosion explosion);
  
  /**
    * Проводит ли блок редстоун-сигнал?
	* @param world Мир, в котором находится блок
	* @param x X-координата блока
	* @param y Y-координата блока
	* @param z Z-координата блока
	* @param side Сторона блока
    * @return True, если проводит
    */
  public boolean isProvidingRedstonePower(IWorld world, int x, int y, int z, int side);
  
  /**
   * Эффективен ли инструмент для блока? <br>
   *  - Эффективный инструмент ломает блок с полной скоростью (как кирка для камня)
   * @param type Тип инструмента в виде строки, например, "axe" - топор
   * @param metadata Метадата блока
   * @return Эффективен ли инструмент?
   */
  public boolean isToolEffective(String type, int metadata);
  
}
