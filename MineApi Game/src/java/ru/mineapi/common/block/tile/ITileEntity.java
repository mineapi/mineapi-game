package ru.mineapi.common.block.tile;

import ru.mineapi.common.nbt.INBTTagCompound;
import ru.mineapi.common.world.IWorld;

/**
 * Кастомная логика для блока <br>
 * При необходимости добавить кастомную логику блоку, например, печке или сундуку, используется TileEntity. 
 * @author swayfarer
 *
 */
public interface ITileEntity {

	/**
	 * Получить мир энтити
	 * @return Автоматически прикастованный мир
	 */
	public <T extends IWorld> T getWorld();
	
	/** Обновление тайла. Вызывается раз в тик */
	public void onUpdate();
	
	/** Получить X-координату тайла */
	public int getX();
	
	/** Получить Y-координату тайла */
	public int getY();
	
	/** Получить Z-координату тайла */
	public int getZ();
	
	/**
	 * Задать X-координату тайла 
	 * @param x Значение координаты 
	 * @return Автоматически прикастованный this
	 */
	public <T extends ITileEntity> T setX(int x);
	
	/**
	 * Задать Y-координату тайла 
	 * @param y Значение координаты 
	 * @return Автоматически прикастованный this
	 */
	public <T extends ITileEntity> T setY(int y);
	
	/**
	 * Задать Z-координату тайла 
	 * @param z Значение координаты 
	 * @return Автоматически прикастованный this
	 */
	public <T extends ITileEntity> T setZ(int z);
	
	/** 
	 * Записать информацию о тайле в NBT
	 * @param tag NBT
	 */
	public void writeToNBT(INBTTagCompound tag);
	
	/**
	 * Прочитать информацию о тайле из NBT
	 * @param tag Читаемый NBT
	 */
	public void readFromNBT(INBTTagCompound tag);
	
}
