package ru.mineapi.common.block;

import java.util.List;
import java.util.Random;

import lombok.Getter;
import ru.mineapi.common.block.material.BlockMatetrial;
import ru.mineapi.common.entity.IEntity;
import ru.mineapi.common.entity.IEntityPlayer;
import ru.mineapi.common.explosion.IExplosion;
import ru.mineapi.common.world.IWorld;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Абстрактный блок. Базовая реализация {@link IBlock} <br>
 * @author swayfarer
 *
 */
public class AbstractBlock implements IBlock {

	/** Имя блока, под которым он зарегистрирован и по которому выбирается id */
	@InternalElement @Getter
	public String blockName;
	
	/** Непрозрачный ли блок? */
	@InternalElement @Getter
	public boolean isOpacueCube;
	
	/** Полный ли это блок? */
	@InternalElement @Getter
	public boolean isNormalBlock;
	
	/** Может ли быть блок скопан при помощи "Шелкового касания" */
	@InternalElement @Getter
	public boolean canSilkTouch;
	
	/** Уровень света, испускаемый блоком */
	@InternalElement @Getter
	public int lightValue;
	
	/** Материал блока */
	@InternalElement @Getter
	public BlockMatetrial material;
	
	/** Трудность поломки блока */
	@InternalElement
	public float hardness;

	@Override
	public float getBlockHardness(IWorld IWorld, int x, int y, int z)
	{
		return hardness;
	}

	@Override
	public void updateTick(IWorld world, int x, int y, int z, Random rand)
	{
		
	}

	@Override
	public void randomDisplayTick(IWorld world, int x, int y, int z, Random rand)
	{
		
	}

	@Override
	public void onBlockDestroyedByPlayer(IWorld world, int x, int y, int z, int meta)
	{
		
	}

	@Override
	public void onNeighborBlockUpdated(IWorld world, int x, int y, int z, IBlock changedBlock)
	{
		
	}

	@Override
	public void onBlockAdded(IWorld world, int x, int y, int z)
	{
		
	}

	@Override
	public void onBlockBreaked(IWorld world, int x, int y, int z, IBlock block, int meta)
	{
		
	}

	@Override
	public void onEntityCollidedWithBlock(IWorld world, int x, int y, int z, int meta, IEntity entity)
	{
		
	}

	@Override
	public void onBlockHarvested(IWorld world, IEntityPlayer player, int x, int y, int z, int fortuneLevel)
	{
		
	}

	@Override
	public boolean canBlockStay(IWorld world, int x, int y, int z)
	{
		return true;
	}

	@Override
	public int getRedstonePower(IWorld world, int x, int y, int z, int side)
	{
		return 0;
	}

	@Override
	public boolean canSilkTouch()
	{
		return canSilkTouch;
	}

	@Override
	public String getDisplayName()
	{
		return getBlockName();
	}

	@Override
	public void addSubBlocks(List<?> subBlocksList)
	{
		
	}

	@Override
	public void onBlockExploded(IWorld world, int x, int y, int z, IExplosion explosion) {}

	@Override
	public boolean isProvidingRedstonePower(IWorld world, int x, int y, int z, int side)
	{
		return false;
	}

	@Override
	public boolean isToolEffective(String type, int metadata)
	{
		return false;
	}

}
