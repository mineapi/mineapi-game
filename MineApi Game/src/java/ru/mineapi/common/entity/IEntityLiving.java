package ru.mineapi.common.entity;

/**
 * Живая энтити. <br>
 * Всякие мобы, игроки и пр.
 * @author swayfarer
 *
 */
public interface IEntityLiving extends IEntity {

}
