package ru.mineapi.common.entity;

import ru.mineapi.common.world.IWorld;

/**
 * Энтити <br> 
 * Кастомные сущности в мире реализованы через энтити. <br>
 * Всякие мобы (для них есть {@link IEntityLiving}), стрелы, дропнутые предметы и пр - это энтити. Они повсюду =)
 * @author swayfarer
 *
 */
public interface IEntity {

	/** Получить мир */
	public <T extends IWorld> T getWorld();
	
	/** Получить X-координату энтити */
	public double getX();
	
	/** Получить Y-координату энтити */
	public double getY();
	
	/** Получить Z-координату энтити */
	public double getZ();
	
	/** Обновление энтити, вызывается каждый тик */
	public void onUpdate();
	
	/**
	 * Задать X-координату энтити
	 * @param x Значение координаты 
	 * @return Энтити (this) после изменений
	 */
	public <T extends IEntity> T setX(double x);
	
	/**
	 * Задать Y-координату энтити
	 * @param y Значение координаты 
	 * @return Энтити (this) после изменений
	 */
	public <T extends IEntity> T setY(double y);
	
	/**
	 * Задать Z-координату энтити
	 * @param z Значение координаты 
	 * @return Энтити (this) после изменений
	 */
	public <T extends IEntity> T setZ(double z);
}
