package ru.mineapi.common.world;

import ru.mineapi.common.block.IBlock;
import ru.mineapi.common.block.tile.ITileEntity;

/**
 * Интерфейс для взаимодействия с миром
 * @author swayfarer
 *
 */
public interface IWorld {

	/**
	 * Задать медатадату блока на указанной позиции
	 * @param x X-координата блока
	 * @param y Y-координата блока 
	 * @param z Z-координата блока 
	 * @param meta Метадата
	 */
	public void setBlockMeta(int x, int y, int z, int meta);
	
	/**
	 * Задать блок на указанной позиции 
	 * @param x X-координата блока
	 * @param y Y-координата блока 
	 * @param z Z-координата блока 
	 * @param block Блок
	 */
	public void setBlock(int x, int y, int z, IBlock block);
	
	/**
	 * Получить блок на указанной координате <br>
	 * Автоматически кастует на желаемый тип, не поймате {@link ClassCastException} =)
	 * @param x X-координата блока
	 * @param y Y-координата блока 
	 * @param z Z-координата блока 
	 * @return Блок на указанной координате
	 */
	public <T extends IBlock> T getBlock(int x, int y, int z);
	
	/**
	 * Получить тайл на указанной координате <br>
	 * Автоматически кастует на желаемый тип, не поймате {@link ClassCastException} =)
	 * @param x X-координата блока
	 * @param y Y-координата блока 
	 * @param z Z-координата блока 
	 * @return Тайл на указанной координате или null, если не найдется
	 */
	public <T extends ITileEntity> T getTile(int x, int y, int z);
	
	/**
	 * Получить метадату блока на указанной координате <br>
	 * @param x X-координата блока
	 * @param y Y-координата блока 
	 * @param z Z-координата блока 
	 * @return Тайл на указанной координате или null, если не найдется
	 */
	public int getBlockMeta(int x, int y, int z);
}
