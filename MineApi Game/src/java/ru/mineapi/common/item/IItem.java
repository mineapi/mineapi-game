package ru.mineapi.common.item;

import ru.mineapi.common.entity.IEntity;
import ru.mineapi.common.entity.IEntityPlayer;
import ru.mineapi.common.world.IWorld;

/**
 * Предмет
 * @author swayfarer
 *
 */
public interface IItem {

	/** Получить имя предмета, под которым он зарегистрирован */
	public String getItemName();
	
	/** Получить отображаемое имя предмета */
	public String getDisplayName();
	
	/** Получить ID предмета */
	public int getItemID();
	
	/** Получить максимальный размер стака */
	public int getMaxStackSize();
	
	/** Получить максимальное повреждение */
	public int getMaxDamage();
	
	/**
	 * Подходит ли книга для зачарования предмета
	 * @param stack Чаруемый стак
	 * @param book Книга с зачарованием
	 * @return True, если поодходит 
	 */
	public boolean isAcceptsBook(IItemStack stack, IItemStack book);
	
	/** Получить отображаемое имя стака */
	public int getDisplayDamage(IItemStack stack);
	
	/** Показывать ли полоску повреждения для предмета? */
	public boolean showDurabilityBar(IItemStack stack);
	
	/**
	 * Клик правой кнопкой мыши по предмету 
	 * @param stack Стак, по которому происходит клик
	 * @param world Мир, в котором происходит клик
	 * @param player Игрок, который кликнул 
	 * @return Стак, который будет помешен в слот вместо этого 
	 */
	public IItemStack onItemRightClick(IItemStack stack, IWorld world, IEntityPlayer player);
	
	/**
	 * Предмет использован на блоке
	 * @param stack Стак, который используется на блоке
	 * @param player Игрок, который использует предмет
	 * @param world Мир, в котором используется пример
	 * @param x X-координата блока, на котором используется предмет
	 * @param y Y-координата блока, на котором используется предмет
	 * @param z Z-координата блока, на котором используется предмет
	 * @param side Сторона блока, на которой используется предмет
	 * @param clickX X-координата попадания по блоку
	 * @param clickY Y-координата попадания по блоку
	 * @param clickZ Z-координата попадания по блоку
	 * @return True, если предмет используется
	 */
	public boolean onItemUse(IItemStack stack, IEntityPlayer player, IWorld world, int x, int y, int z, int side, float clickX, float clickY, float clickZ);
	
	/**
	 * Предмет находится в инвенторе. Вызывается каждый тик.
	 * @param stack Стак в инвенторе
	 * @param world Мир энтити, в инвенторе которой лежит предмет
	 * @param entity Энтити, в инвенторе которой лежит предмет
	 * @param slot Слот, в котором находится предмет 
	 */
	public void onUpdateInInventory(IItemStack stack, IWorld world, IEntity entity, int slot);
	
	/**
	 * Предмет был создан игроком
	 * @param stack Стак, который был создан 
	 * @param world Мир, в котором предмет был создан 
	 * @param player Игрок, который создал предмет 
	 */
	public void onCrafted(IItemStack stack, IWorld world, IEntityPlayer player);
	
	/**
	 * Прекращено использование предмета 
	 * @param stack Стак, который прекратили использовать
	 * @param world Мир, в котором прекратили использовать предмет
	 * @param player Игрок, который прекратил использовать предмет 
	 */
	public void onItemStoppedUsing(IItemStack stack, IWorld world, IEntityPlayer player);
}
