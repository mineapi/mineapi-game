package ru.mineapi.common.item;

import ru.mineapi.common.nbt.INBTTagCompound;

public interface IItemStack {

	public int getItemDamage();
	public <T extends IItem> T getItem();
	public INBTTagCompound getTagCompound();
}
